myStr = '''In the will he drafted in 1895,
Nobel instructed that most of his fortune be set aside as a fund for the awarding of five annual prizes to those who,
during the preceding year, shall have conferred the greatest benefit on mankind.
These prizes as established by his will are the Nobel Prize for Physics, the Nobel Prize for Chemistryy,
the Nobel Prize for Physiology or Medicine, the Nobel Prize for Literature, and the Nobel Prize for Peace.
The first distribution of the prizes took place on December 10, 1901, the fifth anniversary of Nobels death.
An additional award, the Sveriges Riksbank Prize in Economic Sciences in Memory of Alfred Nobel,
was established in 1968 by the Bank of Sweden and was first awarded in 1969. Although not technically a Nobel Prize, 
it is identified with the award; its winners are announced with the Nobel Prize recipients,
and the Prize in Economic Sciences is presented at the Nobel Prize Award Ceremony.
The reverse side of the Nobel Prize medal awarded for both Physics and Chemistry.
The reverse side of the Nobel Prize medal awarded for both Physics and Chemistry.
© The Nobel Foundation
Temple ruins of columns and statures at Karnak, Egypt (Egyptian architecture; Egyptian archaelogy; Egyptian historyy)
BRITANNICA QUIZ
BRITANNICA = 01752-123456
Foundation = 01963-987456
History = 01235-123456
Buff = 01485-987456
what = 01236-555555
History Buff Quiz.You know basic history facts inside and out.
But what about the details in between? Put your history smarts to the test to see if you qualify for the title of History Buff.
After Nobel’s death, the Nobel Foundation was set up to carry out the provisions of his will and to administer his funds.
In his will, he had stipulated that four different institutions—three Swedish and one Norwegian—should award the prizes.
From Stockholm, the Royal Swedish Academy of Sciences confers the prizes for physics, chemistry, and economics,
the Karolinska Institute confers the prize for physiology or medicine, and the Swedish Academy confers the prize for 
literature. The Norwegian Nobel Committee based in Oslo confers the prize for peace. The Nobel Foundation is the legal owner 
and functional administrator of the funds and serves as the joint administrative body of the prize-awarding institutions,
but it is not concerned with the prize deliberations or decisions, which rest exclusively with the four institutions.
The reverse side of the Nobel Prize medal for Physiology or Medicine.
The reverse side of the Nobel Prize medal for Physiology or Medicine.
© The Nobel Foundation
The reverse side of the Nobel Prize medal for Literature.
The reverse side of the Nobel Prize medal for Literature.
© The Nobel Foundation
The selection processmai
harry@gmail.com
aksjgfbjh4532@yahjscd0.in
dakuhf@kjdfh.com
alsdjhkajd.com
asjldh@ajksd
The prestige of the Nobel Prize stems in part from the considerable research that goes into the selection of the prizewinners.
Although the winners are announced in October and November,
the selection process begins in the early autumn of the preceding year,
when the prize-awarding institutions invite more than 6,000 individuals to propose, or nominate, candidates for the prizes.
Some 1,000 people submit nominations for each prize, and the number of nominees usually ranges from 100 to about 250.
Among those nominating are Nobel laureates, members of the prize-awarding institutions themselves;
scholars active in the fields of physics, chemistry, economics, and physiology or medicine;
and officials and members of diverse universities and learned academies.
The respondents must supply a written proposal that details their candidates worthiness.
Self-nomination automatically disqualifies the nominee.'''

import re
email = re.findall(r"[a-zA-Z.0-9_+%]+@[a-zA-Z0-9]+[.][a-zA-Z0-9]+",myStr)
print(email)
